# Web Example Project #

This an example project about embedding TADOtv Web Player using Iframe.

## Getting Started ##

### How to make TADOtv Web Player fit to screen using iframe using CSS ###

put the meta tag on **<head></head>** like the code below . 

```
<meta name="viewport" content="width=device-width, initial-scale=1.0">
```
This is important thing to gives the browser instructions on how to control the page's dimensions and scaling. Reference : https://www.w3schools.com/css/css_rwd_viewport.asp. 

After that, implement style code like below

```
<style type="text/css">
	body{
		margin: 0;
		padding: 0;
		width: 100%;
		min-height: -webkit-fill-available //to make height fit to screen on chrome, safari and all browser using webkit engine
		min-height: -moz-fill-available //o make height fit to screen on firefox
		min-height: 100vh; //fallback if -webkit-fill-available or -moz-fill-available cannot be used
	}
	iframe{
		position: fixed;
		width: 100%; //to set iframe widht fit to the body
		height: 100%; //to set iframe height fit to the body
	}
</style>
```

### Iframe Embed Code ###

```
<iframe title="tadoTV" src="https://botolkecap.tadotv.com/watch/PGKulsG?noredirect=true" frameBorder="0" allowFullScreen><p>No Support</p></iframe>
```

by default, iframe will set **frameBorder** to 1 so we set it 0 to remove border. **allowFullScreen** attribute to allow player inside iframe can be fullscreen state